package page_objects;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.Utility;

public class MainPage extends BasePageObject {
    private final By buttonListLocator = By.xpath("//android.view.ViewGroup[@resource-id = 'com.google.android.calculator:id/pad_numeric']/android.widget.Button");
    private final By plusButtonLocator = By.id("com.google.android.calculator:id/op_add");
    private final By multiplyButtonLocator = By.id("com.google.android.calculator:id/op_mul");
    private final By subtractButtonLocator = By.id("com.google.android.calculator:id/op_sub");
    private final By divisionButtonLocator = By.id("com.google.android.calculator:id/op_div");
    private final By equalButtonLocator = By.id("com.google.android.calculator:id/eq");
    private final By resultPreviewLabelLocator = By.id("com.google.android.calculator:id/result_preview");
    private final By resultLabelLocator = By.id("com.google.android.calculator:id/result_final");
    private final By tanLabelLocator = By.id("com.google.android.calculator:id/fun_tan");

    public MainPage(AppiumDriver appiumDriver) {
        super(appiumDriver);
    }

    public MainPage addOperation(int argumentOne, int argumentTwo) {
        int buttonIndexOne = Utility.transformNumberToButtonIndex(argumentOne);
        int buttonIndexTwo = Utility.transformNumberToButtonIndex(argumentTwo);

        waitTillDisappear(tanLabelLocator);
        getElementList(buttonListLocator).get(buttonIndexOne).click();
        getElement(plusButtonLocator).click();
        getElementList(buttonListLocator).get(buttonIndexTwo).click();
        getElement(equalButtonLocator).click();
        return this;
    }

    public MainPage multiplyOperation(int argumentOne, int argumentTwo) {
        int buttonIndexOne = Utility.transformNumberToButtonIndex(argumentOne);
        int buttonIndexTwo = Utility.transformNumberToButtonIndex(argumentTwo);

        waitTillDisappear(tanLabelLocator);
        getElementList(buttonListLocator).get(buttonIndexOne).click();
        getElement(multiplyButtonLocator).click();
        getElementList(buttonListLocator).get(buttonIndexTwo).click();
        getElement(equalButtonLocator).click();
        return this;
    }

    public MainPage subtractOperation(int argumentOne, int argumentTwo) {
        int buttonIndexOne = Utility.transformNumberToButtonIndex(argumentOne);
        int buttonIndexTwo = Utility.transformNumberToButtonIndex(argumentTwo);

        waitTillDisappear(tanLabelLocator);
        getElementList(buttonListLocator).get(buttonIndexOne).click();
        getElement(subtractButtonLocator).click();
        getElementList(buttonListLocator).get(buttonIndexTwo).click();
        getElement(equalButtonLocator).click();
        return this;
    }

    public MainPage divisionOperation(int argumentOne, int argumentTwo) {
        int buttonIndexOne = Utility.transformNumberToButtonIndex(argumentOne);
        int buttonIndexTwo = Utility.transformNumberToButtonIndex(argumentTwo);

        waitTillDisappear(tanLabelLocator);
        getElementList(buttonListLocator).get(buttonIndexOne).click();
        getElement(divisionButtonLocator).click();
        getElementList(buttonListLocator).get(buttonIndexTwo).click();
        getElement(equalButtonLocator).click();
        return this;
    }

    public MainPage divisionByZeroOperation(int argumentOne) {
        int buttonIndexOne = Utility.transformNumberToButtonIndex(argumentOne);
        waitTillDisappear(tanLabelLocator);
        getElementList(buttonListLocator).get(buttonIndexOne).click();
        getElement(divisionButtonLocator).click();
        getElementList(buttonListLocator).get(9).click();
        getElement(equalButtonLocator).click();
        return this;
    }

    public WebElement getResultLabel() {
        return getElement(resultLabelLocator);
    }

    public WebElement getResultPreviewLabel() {
        return getElement(resultPreviewLabelLocator);
    }

}
