package functional;

import org.testng.annotations.Test;
import utils.MobileTestRunner;
import utils.Utility;

import static org.testng.Assert.assertEquals;

public class AdditionTest extends MobileTestRunner {

    @Test
    public final void additionTest() {
        int argumentOne = Utility.randomNumber();
        int argumentTwo = Utility.randomNumber();
        String resultExpected = Integer.toString(argumentOne + argumentTwo);

        String resultActual = mainPage.addOperation(argumentOne, argumentTwo)
                .getResultLabel().getText();

        String errorMessage = String.format("Addition result is wrong. Actual:%1$s Expected: %2$s", resultActual, resultExpected);

        assertEquals(resultExpected, resultActual, errorMessage);
    }
}
