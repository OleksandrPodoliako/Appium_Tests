package functional;

import org.testng.annotations.Test;
import utils.MobileTestRunner;
import utils.Utility;

import static org.testng.Assert.assertEquals;

public class DivisionByZeroTest extends MobileTestRunner {

    @Test
    public final void divisionByZeroTest() {
        int argumentOne = Utility.randomNumber();

        String resultFromApp = mainPage
                .divisionByZeroOperation(argumentOne)
                .getResultPreviewLabel()
                .getText();

        String errorMessage = String.format("Division by zero result is wrong. Actual:%1$s Expected: Can't divide by 0", resultFromApp);

        assertEquals(resultFromApp, "Can't divide by 0", errorMessage);
    }
}
