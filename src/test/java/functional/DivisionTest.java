package functional;

import org.testng.annotations.Test;
import utils.MobileTestRunner;
import utils.Utility;

import static org.testng.Assert.assertTrue;

public class DivisionTest extends MobileTestRunner {

    @Test
    public final void divisionTest() {
        int argumentOne = Utility.randomNumber();
        int argumentTwo = Utility.randomNumber();
        double result = (double) argumentOne / argumentTwo;

        String resultFromApp = mainPage
                .divisionOperation(argumentOne, argumentTwo)
                .getResultLabel()
                .getText();

        double subtraction = Math.abs(Double.parseDouble(resultFromApp) - result);

        String errorMessage = String.format("Division result is wrong. Actual:%1$s Expected: %2$s", resultFromApp, result);

        assertTrue(subtraction <= 0.01, errorMessage);
    }
}
