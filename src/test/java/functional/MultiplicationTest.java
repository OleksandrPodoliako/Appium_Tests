package functional;

import org.testng.annotations.Test;
import utils.MobileTestRunner;
import utils.Utility;

import static org.testng.Assert.assertEquals;

public class MultiplicationTest extends MobileTestRunner{

    @Test
    public final void multiplicationTest() {
        int argumentOne = Utility.randomNumber();
        int argumentTwo = Utility.randomNumber();
        String resultExpected = Integer.toString(argumentOne * argumentTwo);

        String resultActual = mainPage
                .multiplyOperation(argumentOne, argumentTwo)
                .getResultLabel()
                .getText();

        String errorMessage = String.format("Multiply result is wrong. Actual:%1$s Expected: %2$s", resultActual, resultExpected);

        assertEquals(resultExpected, resultActual, errorMessage);
    }
}