package functional;

import org.testng.annotations.Test;
import utils.MobileTestRunner;
import utils.Utility;

import static org.testng.Assert.assertEquals;


public class SubtractionTest extends MobileTestRunner {

    @Test
    public final void subtractionTest() {
        int argumentOne = Utility.randomNumber();
        int argumentTwo = Utility.randomNumber();
        int subtraction = argumentOne > argumentTwo ? argumentOne - argumentTwo : argumentTwo - argumentOne;
        String resultExpected = Integer.toString(subtraction);

        String resultActual = mainPage
                .subtractOperation(argumentOne, argumentTwo)
                .getResultLabel()
                .getText();

        String errorMessage = String.format("Subtraction result is wrong. Actual:%1$s Expected: %2$s", resultActual, resultExpected);

        assertEquals(resultExpected, resultActual, errorMessage);
    }
}